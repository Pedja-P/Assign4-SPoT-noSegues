//
//  FlickrTagTVC.h
//  SPoT
//
//  Created by Predrag Pavlovic on 6/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrTagTVC : UITableViewController

@property (copy, nonatomic) NSArray *photos;  // of NSDictionary

- (NSArray *)ignoredTags; // of NSString

@end
