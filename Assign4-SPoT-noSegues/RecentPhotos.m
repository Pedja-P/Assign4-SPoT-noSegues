//
//  RecentPhotos.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/11/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//
//  TVC helper class
//

#import "RecentPhotos.h"
#import "FlickrFetcher.h"

@interface RecentPhotos ()

@end

@implementation RecentPhotos

#define ALL_RECENT_PHOTOS_KEY @"RecentPhotos_All"
#define MAX_NUMBER_OF_PHOTOS 7

+ (NSArray *)allRecentPhotos
{
    NSArray *allRecentPhotos = [[NSUserDefaults standardUserDefaults] arrayForKey:ALL_RECENT_PHOTOS_KEY];
    if (!allRecentPhotos) {
        allRecentPhotos = [[NSArray alloc] init];
    }
    return allRecentPhotos;
}

+ (void)addPhoto:(NSDictionary *)photo
{
    NSMutableArray *allRecentPhotos = [[self allRecentPhotos] mutableCopy];
    NSUInteger indexOfCopy = NSNotFound;
    NSString *newPhotoId = [photo valueForKey:FLICKR_PHOTO_ID];
    for (NSDictionary *oldPhoto in allRecentPhotos) {
        NSString *oldPhotoId = [oldPhoto valueForKey:FLICKR_PHOTO_ID];
        if ([oldPhotoId isEqualToString:newPhotoId]) {
            indexOfCopy = [allRecentPhotos indexOfObject:oldPhoto];
            break;
        }
    }
    if (indexOfCopy == NSNotFound) {
        if ([allRecentPhotos count] == MAX_NUMBER_OF_PHOTOS) {
            [allRecentPhotos removeLastObject];
        }
    } else {
        [allRecentPhotos removeObjectAtIndex:indexOfCopy];
    }
    [allRecentPhotos insertObject:photo atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:allRecentPhotos forKey:ALL_RECENT_PHOTOS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
