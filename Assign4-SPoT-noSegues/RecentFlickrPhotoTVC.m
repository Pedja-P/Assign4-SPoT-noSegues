//
//  RecentFlickrPhotoTVC.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/3/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RecentFlickrPhotoTVC.h"
#import "RecentPhotos.h"

@interface RecentFlickrPhotoTVC ()

@end

@implementation RecentFlickrPhotoTVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.flickrPhotos = [RecentPhotos allRecentPhotos];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
