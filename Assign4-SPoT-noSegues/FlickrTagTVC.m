//
//  FlickrTagTVC.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "FlickrTagTVC.h"
#import "FlickrFetcher.h"

@interface FlickrTagTVC ()

@property (strong, nonatomic) NSArray *tags;    // of NSString
@property (strong, nonatomic) NSMutableDictionary *taggedPhotos;

@end

@implementation FlickrTagTVC

- (void)setPhotos:(NSArray *)photos
{
    _photos = [photos copy];
    [self updateTableContent];
}

- (NSArray *)ignoredTags
{
    return nil;
}

- (NSArray *)tags
{
    if (!_tags) {
        _tags = [[NSArray alloc] init];
    }
    return _tags;
}

- (NSMutableDictionary *)taggedPhotos
{
    if (!_taggedPhotos) {
        _taggedPhotos = [[NSMutableDictionary alloc] init];
    }
    return _taggedPhotos;
}

- (void)updateTableContent
{
    for (NSDictionary *photo in self.photos) {
        NSArray *photoTags = [photo[FLICKR_TAGS] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        for (NSString *tag in photoTags) {
            NSMutableArray *photosWithTag = self.taggedPhotos[tag];
            if (!photosWithTag) {
                photosWithTag = [[NSMutableArray alloc] init];
            }
            [photosWithTag addObject:photo];
            [self.taggedPhotos setObject:photosWithTag forKey:tag];
        }
    }
    [self.taggedPhotos removeObjectsForKeys:self.ignoredTags];
    self.tags = [[self.taggedPhotos allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    [self.tableView reloadData];
}

/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
*/
 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tags count];
}

- (NSString *)titleForRow:(NSUInteger)row
{
    return [[self.tags[row] description] capitalizedString]; 
}

- (NSString *)subtitleForRow:(NSUInteger)row
{
    NSUInteger numberOfPictures = [self.taggedPhotos[self.tags[row]] count];
    return [NSString stringWithFormat:@"%lu photo%@", (unsigned long)numberOfPictures, (numberOfPictures == 1) ? @"" : @"s"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Flickr Tag";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = [self titleForRow:indexPath.row]; 
    cell.detailTextLabel.text = [self subtitleForRow:indexPath.row];
    return cell;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString:@"Show Photos"]) {
                SEL setFlickrPhotosSelector = sel_registerName("setFlickrPhotos:"); // to suppress the warning "undeclared selector"
                // we don't know the class (we haven't imported it), but we know it should respond to message
                if ([segue.destinationViewController respondsToSelector:setFlickrPhotosSelector]) {
                    [segue.destinationViewController performSelector:setFlickrPhotosSelector
                                                          withObject:self.taggedPhotos[self.tags[indexPath.row]]];
                    
                    [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
                }
            }
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     
}
*/

@end
