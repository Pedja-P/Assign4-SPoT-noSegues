//
//  StanfordFlickrTagTVC.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/7/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "StanfordFlickrTagTVC.h"
#import "FlickrFetcher.h"

@interface StanfordFlickrTagTVC ()

@end

@implementation StanfordFlickrTagTVC

- (NSArray *)ignoredTags
{
    return @[@"cs193pspot", @"portrait", @"landscape"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.photos = [FlickrFetcher stanfordPhotos];
}

@end
