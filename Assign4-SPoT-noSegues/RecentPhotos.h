//
//  RecentPhotos.h
//  SPoT
//
//  Created by Predrag Pavlovic on 6/11/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecentPhotos : NSObject

+ (NSArray *)allRecentPhotos;
+ (void)addPhoto:(NSDictionary *)photo;

@end
