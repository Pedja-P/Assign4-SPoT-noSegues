//
//  SplitViewDelegateTVC.h
//  SPoT_PH
//
//  Created by CS193p Instructor.
//  Copyright (c) 2011 Stanford University. All rights reserved.
//
//  Generic.
//  Will play the role of delegate if in a split view.
//  Will do split view bar button dance as long as
//  the detail view controller implements SplitViewBarButtonItemPresenter protocol.
//

#import <UIKit/UIKit.h>

@interface SplitViewDelegateTVC : UITableViewController <UISplitViewControllerDelegate>
- (void)transferSplitViewBarButtonItemToViewController:(id)destinationViewController;

@end
