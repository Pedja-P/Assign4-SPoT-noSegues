//
//  ImageViewController.m
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University. All rights reserved.
//

#import "ImageViewController.h"
#import "AttributedStringViewController.h"

@interface ImageViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView; // we add UIImageView in code, not in storyboard,
// because we dynamicaly get image and we don't know its size (used for content size)
@property (weak, nonatomic) IBOutlet UIBarButtonItem *titleBarButtonItem;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) UIPopoverController *urlPopover;
@property (nonatomic, getter = isAutoZoomed) BOOL autoZoomed;

@end

@implementation ImageViewController

// implementation of SplitViewBarButtonItemPresenter protocol
// property defined in protocol cannot be auto sythesized, we need to do it (or we could declare the @property)
@synthesize splitViewBarButtonItem = _splitViewBarButtonItem;

// sets the title of the titleBarButtonItem (if connected) to the passed VC's title
// (override inherited public setter)
- (void)setTitle:(NSString *)title
{
    super.title = title; // self.title would create infinite loop,
                         // ivars are not accessible from subclass (_title)
    self.titleBarButtonItem.title = title;
}


// resets the image whenever the URL changes
- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    [self resetImage];
}

// fetches the data from the URL
// turns it into an image
// adjusts the scroll view's content size to fit the image
// sets the image as the image view's image
- (void)resetImage
{
    if (self.scrollView) {
        self.scrollView.contentSize = CGSizeZero;
        self.imageView.image = nil;
        self.autoZoomed = YES;
        
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:self.imageURL];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            self.scrollView.zoomScale = 1.0;    // we need to reset zoom before setting content size
            self.scrollView.contentSize = image.size;
            self.imageView.image = image;
            self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
            // upper left corner of scrollView with size of image
            [self fillView];    // because viewDidLayoutSubviews is not called everytime (only on changing orientation) if we don't use replace segue
        }
    }
}

#pragma mark - Popover

// returns whether the "Show URL" segue should be allowed to fire
// prohibits the segue if we don't have a URL set in us yet or
//  if a popover showing the URL is already visible
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender // inherited
{
    if ([identifier isEqualToString:@"Show URL"]) {
        return (self.imageURL && !self.urlPopover.isPopoverVisible) ? YES : NO;
    } else {
        return [super shouldPerformSegueWithIdentifier:identifier sender:sender];
    }
}

// uses an AttributedStringViewController to display the URL of the image we are currently displaying
// if being presented by a Popover segue, grab ahold of the popover so that we can avoid
// putting it up multiple times (by prohibiting it in shouldPerformSegueWithIdentfier:sender:)
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show URL"]) {
        if ([segue.destinationViewController isKindOfClass:[AttributedStringViewController class]]) {
            AttributedStringViewController *asc = (AttributedStringViewController *)segue.destinationViewController;
            asc.text = [[NSAttributedString alloc] initWithString:[self.imageURL description]];
            if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]) {
                self.urlPopover = ((UIStoryboardPopoverSegue *)segue).popoverController;
            }
        }
    }
}

#pragma mark -

// lazy instantiation
- (UIImageView *)imageView
{
    if (!_imageView) _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    // we don't know image yet (and its size), so we use zero size rect
    return _imageView;
}

// returns the view which will be zoomed when the user pinches
// in this case, it is the image view, obviously
// (there are no other subviews of the scroll view in its content area)
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

/* testing different stuff
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"x = %f\ny = %f\nwidth = %f\nheight = %f\n", scrollView.bounds.origin.x, scrollView.bounds.origin.y, scrollView.bounds.size.width, scrollView.bounds.size.height);
    CGRect visible = [scrollView convertRect:scrollView.bounds toView:self.imageView];
    NSLog(@"xA = %f\nyA = %f\nwidthA = %f\nheightA = %f\n", visible.origin.x, visible.origin.y, visible.size.width, visible.size.height);
}
*/

// add the image view to the scroll view's content area
// setup zooming by setting min and max zoom scale
// and setting self to be the scroll view's delegate
// resets the image in case URL was set before outlets (e.g. scroll view) were set
// sets the title of barButton if VC's title is set before outlet (titleBarButtonItem)
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
    self.scrollView.minimumZoomScale = 0.2;
    self.scrollView.maximumZoomScale = 5.0;
    self.scrollView.delegate = self;
    self.titleBarButtonItem.title = self.title;
    [self handleSplitViewBarButtonItem:self.splitViewBarButtonItem];
    [self resetImage];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if (self.imageView) [self fillView];
}

// Disable autoZoom after the user performs a zoom (by pinching)
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    self.autoZoomed = NO;
}

- (void)fillView
{
    if (self.isAutoZoomed) {
        float wScale = self.view.bounds.size.width / self.imageView.bounds.size.width;
        float hScale = self.view.bounds.size.height / self.imageView.bounds.size.height;
        self.scrollView.zoomScale = MAX(wScale, hScale);
    }
}

#pragma mark - SplitViewBarButtonItemPresenter protocol

// Puts the splitViewBarButton in our toolbar (and/or removes the old one).
// Must be called when our splitViewBarButtonItem property changes
// (and also after our view has been loaded from the storyboard - viewDidLoad).
- (void)handleSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    if (_splitViewBarButtonItem) [toolbarItems removeObject:_splitViewBarButtonItem];
    if (splitViewBarButtonItem) [toolbarItems insertObject:splitViewBarButtonItem atIndex:0];
    self.toolbar.items = toolbarItems;
    _splitViewBarButtonItem = splitViewBarButtonItem;
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    if (splitViewBarButtonItem != _splitViewBarButtonItem) {    // drawing is slow, so don't draw new button if it hasn't changed
        [self handleSplitViewBarButtonItem:splitViewBarButtonItem];
    }
}

@end
