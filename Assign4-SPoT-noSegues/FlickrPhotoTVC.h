//
//  FlickrPhotoTVC.h
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University. All rights reserved.
//
//  Will call setImageURL: as part of any "Show Image" segue.

#import <UIKit/UIKit.h>
#import "SplitViewDelegateTVC.h"

// this class is a delegate for SplitView because detail view can only be changed when this view is loaded 
@interface FlickrPhotoTVC : SplitViewDelegateTVC

// the Model for this VC
// an array of dictionaries of Flickr information
// obtained using Flickr API
// (e.g. FlickrFetcher will obtain such an array of dictionaries)
@property (copy, nonatomic) NSArray *flickrPhotos; // of NSDictionary

- (NSArray *)sortDescriptors;   // subclass could use another sort descriptor

@end
